package inf101h22.treedrawer.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import inf101h22.treedrawer.model.ReadableTreeCanvas;

/**
 * The main class of the view.
 * Responsible for drawing the entire application.
 */
public class MainView extends JPanel {
    private static final int GAP = 10;

    private HeadUpDisplay top;
    private TreeCanvasView middle;
    private ButtonsPanel bottom;

    /** Creates a new MainView for the TreeDrawer application. */
    public MainView(ReadableTreeCanvas model) {
        this.setLayout(new BorderLayout(GAP, GAP));
        this.setBorder(new EmptyBorder(GAP, GAP, GAP, GAP));

        this.top = new HeadUpDisplay(model.getMessage());
        this.middle = new TreeCanvasView(model);
        this.bottom = new ButtonsPanel();

        this.add(this.top, BorderLayout.NORTH);
        this.add(this.middle, BorderLayout.CENTER);
        this.add(this.bottom, BorderLayout.SOUTH);
    }

    /** Gets the panel that paints the TreeCanvas model */
    public TreeCanvasView getTreeCanvasView() {
        return middle;
    }

    /** Gets the button for reset */
    public JButton getResetButton() {
        return this.bottom.getResetButton();
    }

    /** Gets the button for undo */
    public JButton getUndoButton() {
        return this.bottom.getUndoButton();
    }

    /** Gets the toggle button for auto mode */
    public JToggleButton getAutoButton() {
        return this.bottom.getAutoButton();
    }
    
}
